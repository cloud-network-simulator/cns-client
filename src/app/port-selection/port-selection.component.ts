import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Device} from '../Device';
import {Port} from '../Port';

@Component({
  selector: 'app-port-selection',
  templateUrl: './port-selection.component.html',
  styleUrls: ['./port-selection.component.css']
})
export class PortSelectionComponent implements OnInit {

  private portAname: string;
  private portZname: string;

  private selectedPorts: Map<number, Port> = new Map<number, Port>();

  @Input() private deviceA: Device;
  @Input() private deviceZ: Device;

  @Output() portEmitter = new EventEmitter<Map<number, Port>>();

  constructor() {  }

  ngOnInit() {
  }

  private onSubmit() {
    if (this.portAname && this.portZname) {
      const portA = this.deviceA.getPortByName(this.portAname);
      const portZ = this.deviceZ.getPortByName(this.portZname);
      portA.linked = true;
      portZ.linked = true;
      this.selectedPorts.set(portA.id, portA);
      this.selectedPorts.set(portZ.id, portZ);
      this.portEmitter.emit(this.selectedPorts);
    }
  }
}

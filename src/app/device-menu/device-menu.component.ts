import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Device} from '../Device';
import {Item} from '../Item';
import {Link} from '../Link';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatIconRegistry, MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material';
import {Observable} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import {Port} from '../Port';


@Component({
  selector: 'app-device-menu',
  templateUrl: './device-menu.component.html',
  styleUrls: ['./device-menu.component.css']
})
export class DeviceMenuComponent implements OnInit {

  private eventsSubscription: any;

  @Input() item: Item;
  @Input() treeChange: Observable<DataNode[]>;

  @Output() onDeleteClicked = new EventEmitter<Item>();
  @Output() private portConfEmitter = new EventEmitter<Port>();

  /* tree segment */
  private _transformer = (node: DataNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<FlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  /*tree segment ends */
  device: Device;
  link: Link;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'cog-wheel',
      sanitizer.bypassSecurityTrustResourceUrl('src/assets/images/icons/settings_applications-24px.svg'));

    this.dataSource.data = [];
  }

  hasChild = (_: number, node: FlatNode) => node.expandable;

  ngOnInit() {
    this.eventsSubscription = this.treeChange.subscribe(treeData => this.dataSource.data = treeData);
  }

  isDevice(item: Item) {
    return item instanceof Device;
  }

  isLink(item: Item) {
    return item instanceof Link;
  }

  asDevice(item: Item): Device {
    return item as Device;
  }

  asLink(item: Item): Link {
    return item as Link;
  }

  delete() {
    this.onDeleteClicked.emit();
  }

  isRoot(node: DataNode) {
    return node.name === 'Ports';
  }

  isLinkedPort(node: DataNode) {
    if (node.name !== 'Ports' &&
        this.getPortByName(node.name)) {
        return this.getPortByName(node.name).linked;
      }
    return false;
  }

  private getPortByName(portName: string) {
    for (const port of this.asDevice(this.item).ports) {
      if (port.name === portName) {
        return port;
      }
    }
    return null;
  }

  onPortClick(clickedName: string) {
    this.portConfEmitter.emit(this.asDevice(this.item).getPortByName(clickedName));
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RouterModule } from '@angular/router';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';

import { WorkspaceComponent} from './workspace/workspace.component';
import { PaletteComponent } from './palette/palette.component';
import { WorkbenchComponent } from './workbench/workbench.component';
import { DeviceMenuComponent } from './device-menu/device-menu.component';

import {
  MatButtonModule, MatCardModule,
  MatCheckboxModule, MatFormField, MatFormFieldModule, MatIconModule, MatInputModule,
  MatListModule, MatRadioModule, MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatToolbarModule, MatTreeModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FrontPageComponent } from './frontpage/frontpage.component';
import { ConsoleComponent } from './console/console.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PortSelectionComponent } from './port-selection/port-selection.component';
import { PortConfigComponent } from './port-config/port-config.component';

@NgModule({
  declarations: [
    AppComponent,
    DeviceMenuComponent,
    PaletteComponent,
    WorkspaceComponent,
    WorkbenchComponent,
    FrontPageComponent,
    ConsoleComponent,
    PortSelectionComponent,
    PortConfigComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    MatToolbarModule,
    BrowserModule,
    AppRoutingModule,
    DragDropModule,
    RouterModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatListModule,
    MatButtonModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatRadioModule,
    MatTreeModule,
    MatSelectModule,
  ],
  entryComponents: [ConsoleComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }


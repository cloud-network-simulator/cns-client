import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import {Item} from '../Item';

@Component({
  selector: 'app-palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.css']
})
export class PaletteComponent implements OnInit {

  items: Item[] = [
    {id: 1, typeName: 'switch', isActive: false},
    {id: 2, typeName: 'router', isActive: false},
    {id: 3, typeName: 'link', isActive: false}
  ];
  @Output() onClicked = new EventEmitter<Item>();

  constructor() { }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.items, event.previousIndex, event.currentIndex);
  }

  ngOnInit() {}

  addNewDevice(clickedItem: Item) {
    this.onClicked.emit(clickedItem);
  }

  isSwitch(item: Item) {
    return item.typeName === 'switch';
  }

  isRouter(item: Item) {
    return item.typeName === 'router';
  }

  isLink(item: Item) {
    return item.typeName === 'link';
  }
}
